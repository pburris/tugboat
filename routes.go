package main

import (
	"html/template"
	"net/http"
)

func indexRoute(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/index.html")
	HandleError(err)
	containers := GetAllRunning()
	t.Execute(w, containers)

}

func newContainerRoute(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		runType := r.Form["runtype"][0]

		if runType == "automatic" {
			environment := r.Form["environment"][0]
			command := r.Form["command"][0]
			ports := r.Form["port"][0]
			RunContainer(environment, command, ports)
		}

		if runType == "dockerfile" {
			dockerfile := r.Form["dockerfile"][0]
			tag := r.Form["tag"][0]
			ports := r.Form["port"][0]
			ContainerFromDockerfile(dockerfile, tag, ports)
		}

		http.Redirect(w, r, "/", 301)

	} else {
		t, err := template.ParseFiles("templates/newcontainer.html")
		HandleError(err)

		images := GetImageList()
		t.Execute(w, images)
	}

}

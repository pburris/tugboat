package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	os.Setenv("DOCKER_API_VERSION", "1.35")
	fmt.Println("\nListening on port 8080")

	http.HandleFunc("/", indexRoute)
	http.HandleFunc("/new", newContainerRoute)

	err := http.ListenAndServe(":8080", nil)
	HandleError(err)

}

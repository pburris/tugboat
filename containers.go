package main

import (
	"archive/tar"
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"golang.org/x/net/context"
)

func GetAllRunning() []types.Container {
	cli, err := client.NewEnvClient()
	HandleError(err)

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	HandleError(err)

	return containers
}

func RunContainer(environment string, command string, ports string) {
	ctx := context.Background()
	cli, err := client.NewEnvClient()
	HandleError(err)

	cli.ImagePull(ctx, environment, types.ImagePullOptions{})
	containerConfig := &container.Config{Image: environment}
	hostConfig := &container.HostConfig{}

	if len(command) > 0 {
		containerConfig.Cmd = strings.Split(command, " ")
	}

	if len(ports) > 0 {
		hostPort := nat.Port("0")
		exposedPort := nat.Port("0")

		if strings.Contains(ports, ":") {
			_ports := strings.Split(ports, ":")
			hostPort = nat.Port(_ports[0])
			exposedPort = nat.Port(_ports[1])
		}

		portmap := &nat.PortMap{hostPort: []nat.PortBinding{
			nat.PortBinding{HostIP: "0.0.0.0", HostPort: string(hostPort)},
		}}

		exposedmap := &nat.PortSet{exposedPort: {}}
		hostConfig.PortBindings = *portmap
		containerConfig.ExposedPorts = *exposedmap
	}

	resp, err := cli.ContainerCreate(ctx, containerConfig, hostConfig, nil, "")
	HandleError(err)

	cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{})
}

func ContainerFromDockerfile(df string, name string, ports string) {
	ctx := context.Background()
	cli, err := client.NewEnvClient()
	HandleError(err)

	buf := new(bytes.Buffer)
	tw := tar.NewWriter(buf)
	defer tw.Close()

	dockerFile := "Dockerfile"
	var r io.Reader
	r = strings.NewReader(df)

	readDockerFile, err := ioutil.ReadAll(r)
	HandleError(err)

	tarHeader := &tar.Header{
		Name: dockerFile,
		Size: int64(len(readDockerFile)),
	}
	err = tw.WriteHeader(tarHeader)
	HandleError(err)

	_, err = tw.Write(readDockerFile)
	HandleError(err)

	dockerFileTarReader := bytes.NewReader(buf.Bytes())

	imageBuildResponse, err := cli.ImageBuild(
		ctx,
		dockerFileTarReader,
		types.ImageBuildOptions{
			Tags:       []string{name},
			Context:    dockerFileTarReader,
			Dockerfile: dockerFile,
			Remove:     true})
	HandleError(err)

	defer imageBuildResponse.Body.Close()
	_, err = io.Copy(os.Stdout, imageBuildResponse.Body)
	HandleError(err)

	RunContainer(name, "", ports)
}

func GetImageList() []string {
	ctx := context.Background()
	cli, err := client.NewEnvClient()
	HandleError(err)

	images, err := cli.ImageList(ctx, types.ImageListOptions{All: true})
	HandleError(err)

	imageList := []string{}
	for _, image := range images {
		if len(image.RepoTags) > 0 {
			if image.RepoTags[0] != "<none>:<none>" {
				imageList = append(imageList, image.RepoTags[0])
			}
		}
	}

	return imageList
}
